let config = require('./connection.defaultConfig.js')

try {
    config = require('./connection.config.js')
}
catch (e) {
    if (e instanceof Error && e.code === "MODULE_NOT_FOUND")
        console.log("Cannot load connection.config.js")
    else
        throw e;
}

let pool = require('mysql2').createPoolPromise(config)
// console.log('pool', pool)

module.exports = pool