let bluebird = require('bluebird')

module.exports = {
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'where2buy',
    multipleStatements: true,
    Promise: bluebird
}