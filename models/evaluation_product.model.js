let pool = require( __dirname + './../config/poolAsycConnection.js')
let moment = require('moment')

module.exports = {
    doGet: function (params) {
        let query = 'SELECT (SUM(vote) /(SELECT COUNT(*) FROM evaluation_product WHERE Product_id = ?))*100 AS total FROM evaluation_product WHERE Product_id = ?'
        let queryParams = [params.id,params.id]
        return pool
            .getConnection()
            .then(conn => {
                let res = conn.query(query, queryParams)
                conn.release()
                return res
            })
    },

    doPost: function (body) {
        let query = 'INSERT INTO evaluation_product (date, vote, Product_id, User_id) VALUES (?, ?, ?, ?); '+
                    'SELECT (SUM(vote) /(SELECT COUNT(*) FROM evaluation_product WHERE Product_id = ?))*100 AS total FROM evaluation_product WHERE Product_id = ?'
        let currentDate = moment().format('YYYY-MM-DD')
        let queryParams = [currentDate,body.vote, body.Product_id, body.User_id,body.Product_id,body.Product_id]
        return pool
            .getConnection()
            .then(conn => {
                let res = conn.query(query, queryParams)
                conn.release()
                return res
            })        
    }
}
