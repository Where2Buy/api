let pool = require( __dirname + './../config/poolAsycConnection.js')
let moment = require('moment')

module.exports = {
    doGet: function (params) {
        let query = 'SELECT *, (SELECT (SUM(vote) /(SELECT COUNT(*) FROM evaluation_product WHERE Product_id = product.id))*100 AS total FROM evaluation_product WHERE Product_id = product.id) AS total FROM product WHERE name = ?'
        let queryParams = [params.name]
        return pool
            .getConnection()
            .then(conn => {
                let res = conn.query(query, queryParams)
                conn.release()
                return res
            })
    },

    doPost: function (body) {
      /*  let query =   'INSERT INTO product (name, longitude, latitude, User_id, creation_date) VALUES (?, ?, ?, ?, ?);'+
                      'SET @product_id = LAST_INSERT_ID();'+
                      'INSERT INTO evaluation_product (date,vote,Product_id,User_id) VALUES (?,1,@product_id ,?)'*/
                      
        let query = "CALL add_product(?,?, ?, ?,?, ?);"
        let currentDate = moment().format('YYYY-MM-DD')
        
        //let queryParams = [body.name, body.longitude, body.latitude, body.User_id, currentDate, currentDate, body.User_id]
        let queryParams = [ body.latitude,body.longitude,body.distance, body.User_id, body.name, currentDate]
        return pool
            .getConnection()
            .then(conn => {
                let res = conn.query(query, queryParams)
                conn.release()
                return res
            })        
    }
}
