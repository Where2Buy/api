// let connection = require('./config/connection.js')
// let asycConnection = require('./config/asycConnection.js')
let pool = require( __dirname + './../config/poolAsycConnection.js')

module.exports = {
    doGet: function () {
        let query = 'SELECT name FROM user where id = ?'
        let queryParams = [2]
        return pool
            .getConnection()
            .then(conn => {
                let res = conn.query(query, queryParams)
                conn.release()
                return res
            })
    },

    doPost: function (body) {
        let query = 'INSERT INTO user (email, name, date_first_login, banned, admin, language) VALUES (?, ?, ?, ?, ?, ?)'
        let queryParams = [body.email, body.name, body.date_first_login, body.banned, body.admin, body.language]
        return pool
            .getConnection()
            .then(conn => {
                let res = conn.query(query, queryParams)
                conn.release()
                return res
            })        
    },

    doPut: function (body, params) {
        let query = 'UPDATE user SET name = ? WHERE id = ?'
        let queryParams = [body.name, params.id]
        return pool
            .getConnection()
            .then(conn => {
                let res = conn.query(query, queryParams)
                conn.release()
                return res
            })
    },

    doDelete: function (params) {
        let query = 'DELETE FROM user WHERE id = ?'
        let queryParams = [params.id]
        return pool
            .getConnection()
            .then(conn => {
                let res = conn.query(query, queryParams)
                conn.release()
                return res
            })
    }
}
