let pool = require( __dirname + './../config/poolAsycConnection.js')
let moment = require('moment')

module.exports = {
    doGet: function (params) {
        let query = 'SELECT * FROM user where id = ?'
        let queryParams = [params.id]
        return pool
            .getConnection()
            .then(conn => {
                let res = conn.query(query, queryParams)
                conn.release()
                return res
            })
    },

    doInsertOrUpdate: function (body, params) {
        let query = 'INSERT INTO user (id, date_first_login, banned, admin, language) VALUES(?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE banned=?, admin=?, language=?'
        let currentDate = moment().format('YYYY-MM-DD')
        let queryParams = [params.id, currentDate, body.banned, body.admin, body.language, body.banned, body.admin, body.language]
        return pool
            .getConnection()
            .then(conn => {
                let res = conn.query(query, queryParams)
                conn.release()
                return res
            })
    }
}
