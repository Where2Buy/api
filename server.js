let compression = require('compression')
let express = require('express')
let bodyParser = require('body-parser')
let morgan = require('morgan')

let app = express()
let port = process.env.PORT || 3000

app.use(compression())
// app.set('view cache', true)
app.use(bodyParser.urlencoded({'extended':'true'}))
app.use(bodyParser.json())
app.use(morgan('dev'))

let crud = require('./routes/crud.routes.js')
app.use('/crud', crud)

let user = require('./routes/user.routes.js')
app.use('/user', user)

let product = require('./routes/product.routes.js')
app.use('/product', product)

let evaluation_product = require('./routes/evaluation_product.routes.js')
app.use('/evaluation_product', evaluation_product)

app.listen(port, function (err) {
    if (err) {
        throw err
    }

    console.log('Server started on port ' + port)
})