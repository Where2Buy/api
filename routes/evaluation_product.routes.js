let express = require('express')
let router = express.Router()

let product = require('./../models/evaluation_product.model.js')

router.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now())
    next()
})


router.get('/:id', function(req, res) {
    product.doGet(req.params)
        .then(rows => res.json(rows[0][0]))
        .catch(err => console.log(err))
})

router.post('/', function(req, res) {
    product.doPost(req.body)
        .then(rows => res.json(rows[0][1][0]))
        .catch(err => console.log(err))
})

module.exports = router