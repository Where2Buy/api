let express = require('express')
let router = express.Router()

let crud = require('./../models/crud.model.js')

router.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now())
    next()
})


router.get('/', function(req, res) {
    crud.doGet()
        .then(rows => res.json(rows[0]))
        // .catch(console.error)
        .catch(err => console.log(err))
})

router.post('/', function(req, res) {
    crud.doPost(req.body)
        .then(rows => res.json(rows))
        .catch(err => console.log(err))
})

router.put('/:id', function(req, res) {
    crud.doPut(req.body, req.params)
        .then(rows => res.json(rows))
        .catch(err => console.log(err))
})

router.delete('/:id', function(req, res) {
    crud.doDelete(req.params)
        .then(rows => res.json(rows))
        .catch(err => console.log(err))
})

module.exports = router