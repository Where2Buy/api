let express = require('express')
let _ = require('lodash')

let router = express.Router()

let product = require('./../models/product.model.js')

router.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now())
    next()
})


router.get('/:name', function(req, res) {
    product.doGet(req.params)
        .then(rows => res.json(rows[0]))
        .catch(err => console.log(err))
})

router.post('/', function(req, res) {
    let i = 0
    product.doPost(req.body)
        .then(rows => {
          res.json(_.keyBy(rows[0],function() {
            return i++;
            }))
        })
        .catch(err => console.log(err))
})

module.exports = router