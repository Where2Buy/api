let express = require('express')
let router = express.Router()

let user = require('./../models/user.model.js')

router.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now())
    next()
})


router.get('/:id', function(req, res) {
    user.doGet(req.params)
        .then(rows => res.json(rows[0]))
        .catch(err => console.log(err))
})

router.put('/insert_or_update/:id', function(req, res) {
    user.doInsertOrUpdate(req.body, req.params)
        .then(rows => res.json(rows[0]))
        .catch(err => console.log(err))
})

module.exports = router